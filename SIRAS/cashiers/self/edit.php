<?php
      session_start();
       if (!isset($_SESSION['usr_rol'])) {
       header('Location: ../../index.php'); 
          exit();
      }else{
     if ($_SESSION['usr_rol'] == 'Usr') {
        #  If user isnt't admin can't see this page
      if(!isset($_SESSION['usuario'])) 
      {
          header('Location: ../../index.php'); 
          exit();
      }
    }elseif ($_SESSION['usr_rol'] == 'Admin') {
      header('Location: ../../index.php'); 
          exit();
    }

}
      ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="SIRAS_SALES SYSTEM">
    <meta name="author" content="Avantec - elucio">
    <link rel="icon" href="">
    <title>SIRAS</title>
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://getbootstrap.com/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../../css/dashboard.css" rel="stylesheet">
    <link href="../../css/main.css" rel="stylesheet">
    <!-- Normalize styles -->
    <link href="../../css/normalize.css" rel="stylesheet">
    <script src="../../js/jquery-3.1.1.min.js"></script>
    <script src="../../js/jquery-migrate-3.0.0.js"></script>
    <!-- data table-->
     <script type="text/javascript" src="../../js/tables/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../js/tables/jquery.dataTables.min.css"/>
     <script type="text/javascript" src="../../js/tables/dataTables.buttons.min.js"></script>
     <script type="text/javascript" src="../../js/tables/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../js/tables/vfs_fonts.js"></script>
    <script type="text/javascript" src="../../js/tables/js/buttons.html5.min.js"></script>
  </head>
  <body>
   
        <div class="container">
          <h1 class="page-header">Usuarios</h1>
          <div class="row placeholders">
            
              <!--contenido-->
          
              <form method="post" action = '' class="form-horizontal">

                 <div class="form-group">    
                  <label class="control-label col-sm-2">Usuario</label>
                  <div class="col-sm-10"> 
                    <input class="form-control" name="usr" value=<?php echo $usuario?> type="text" readonly required>
                  </div>
                </div>

                 <div class="form-group">   
                  <label class="control-label col-sm-2">Contraseña</label>
                  <div class="col-sm-10"> 
                    <input class="form-control"  name = "pwd" value=<?php echo $pwd?> type='password' readonly required>
                  </div>
                </div>
                <div class="form-group">   
                  <label class="control-label col-sm-2">Caja</label>
                  <div class="col-sm-10"> 
                    <input class="form-control"  name = "caja" value=<?php echo $caja?>  type="number" required>
                  </div>
                </div>

                <div class="form-group">
                <label class="control-label col-sm-2">Rol</label>
                
                <div class="col-sm-10">
                
                  <select class="form-control" name="rol">
                   <option alue=<?php echo $rol?>><?php echo $rol?></option>
                    <option value="Admin">Administrador</option>
                    <option value="Usr">Cajero</option>
                  </select>
                  </div>
               
                </div>

                  <button name='save' type="submit" class="btn btn-primary btn-block">Salvar 
                  </button>
                  
               
            </form>
          
            
            
            
            </div>
        </div>
   
    <!-- Modal -->


   

 
 
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/main.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>